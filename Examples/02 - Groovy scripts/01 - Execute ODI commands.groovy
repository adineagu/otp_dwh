/*
	Create ODI datastore using Groovy and execution API
*/

def stmt = """
DROP DATASTORE IF EXISTS HR_SOURCE.COUNTRIES_FILE;

CREATE DATASTORE IF NOT EXISTS HR_SOURCE.COUNTRIES_FILE
ALIAS COUNTRIES
RESOURCE 'COUNTRIES_FILE.csv'
(
    COUNTRY_ID  STRING(50)  
   ,COUNTRY_NAME  STRING(50)  
   ,REGION_ID  STRING(50)  
   ,CURRENCY_CD  STRING(50)  
)
FILE (
   FORMAT DELIMITED
   HEADING 1
   RECORD SEPARATOR '\u000D\u000A'
   FIELD SEPARATOR ','
   TEXT DELIMITER '"'
   DECIMAL SEPARATOR '.'
)
;

ALTER DATASTORE HR_SOURCE.COUNTRIES_FILE COMMENT 'Countries source file';
"""

// execute only the next line if you only want to validate the statement (i.e. only a check whether all
// the objects referenced in statement exist).
execodi.validate(stmt)

// execute the statement
execodi.execute(stmt)
