ODI Generator provides an integrated Groovy shell which can be used to automatize repetitive tasks.
ODI Generator expose the internal API to user using the following objects available in Groovy shell:

	- odiInstance: similar to odiInstance variable in ODI Studio's Groovy console
	- metadata: provides access to ODI Generator repository (i.e. retrieve datastores, templates, etc.)
	- codegen: provides access to "apply template" feature
	- execodi: provides access to ODI execution engine
