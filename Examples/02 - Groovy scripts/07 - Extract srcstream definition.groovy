import groovy.sql.Sql
import groovy.sql.GroovyResultSet

import oracle.odi.domain.model.finder.IOdiDataStoreFinder
import oracle.odi.domain.model.OdiDataStore

import org.apache.commons.lang.StringEscapeUtils;


def STREAMS_LIST = ['KDBADREX', 'KDBKUND',]                 // The list of source streams for which you want to create the definition file. 

// datastore finder
DATASTORE_FINDER = (IOdiDataStoreFinder) odiInstance.getTransactionalEntityManager().getFinder(OdiDataStore.class)

// Target database connection parameters
Map dbConnParams = [
  url: 'jdbc:oracle:thin:@localhost:10042/cogrmd.agora',    // change according to your Agora configuration and environment where from you read the definition
  user: 'gdsm_at',
  password: '<enter the password here>',                    // replace <enter the password here> with the actual password
  driver: 'oracle.jdbc.OracleDriver'
]

//SQL statement used to extract source stream definition
EXTRACT_DEF_STMT = """
select gdsm_at.get_srcstream_generator_ddl(
          p_in_srcstream_cd => :srcstreamCd,
          p_in_src_file_format => :fileFormat,
          p_in_src_file_record_sep => :recordSep,
          p_in_src_file_field_sep => :fieldSep,
          p_in_src_file_header_lines => :headerLines,
          p_in_src_file_text_delim => :textDelim,
          p_in_src_file_num_char => :decSep
       ) ddl from dual
"""

DEST_DIR = "C:\\Data\\Projects\\DLAT\\Metadata\\Documents\\dlat\\datastore\\base\\" // change according to your DLAT project home folder

def getDatastoreProperties(model, name) {
  fileFormat = ""
  def headerLines
  recordSep = ""
  fieldSep = ""
  textDelim = ""
  decSep = ""  

  odiDs = DATASTORE_FINDER.findByName(name, model);

  if (!odiDs) {
    [success:false, properties:null]
  } else {
    if (odiDs.getModel().getTechnology().isFileTechnology()) {
      fDesc = odiDs.getFileDescriptor()
      headerLines = fDesc.getSkipHeadingLines()

      recordSep = fDesc.getRowSeparator()
      if (recordSep) {
        if (!recordSep.startsWith("\\u")) {
          recordSep = StringEscapeUtils.escapeJava(fDesc.getRowSeparator())
        } 
      }

      fieldSep = fDesc.getFieldSeparator()
      if (fieldSep) {
        if (!fieldSep.startsWith("\\u")) {
          fieldSep = StringEscapeUtils.escapeJava(fDesc.getFieldSeparator())
        }
      }

      textDelim = fDesc.getTextDelimiter()
      decSep = fDesc.getDecimalSeparator()
      fileFormat = fDesc.getFormat().name()

      println "File format: $fileFormat, Record separator: $recordSep, Field separator: $fieldSep, Text delimiter: $textDelim, Header lines: $headerLines, Decimal separator: $decSep"

      [success:true, properties:[fileFormat:fileFormat, headerLines:headerLines, recordSep:recordSep, fieldSep:fieldSep, textDelim:textDelim, decSep:decSep]]
    } else {
      [success:true, properties:null]
    }
  }
}

def createSrcstreamDefFile(sql, srcstreamcode, datastoreProperties) {
  fileName = srcstreamcode.toLowerCase() + ".sql";
  
  directory = new File(DEST_DIR);
  if (!directory.exists()){
    directory.mkdir();
  }                  
          
  File file = new File(DEST_DIR + File.separator + srcstreamcode[0..2] + File.separator + fileName)
            
  println "Generate source stream " +  srcstreamcode.toUpperCase()

  map = [
          'srcstreamCd':srcstreamcode.toUpperCase(), 
          'fileFormat':datastoreProperties.fileFormat, 
          'recordSep':datastoreProperties.recordSep, 
          'fieldSep':datastoreProperties.fieldSep, 
          'headerLines':datastoreProperties.headerLines, 
          'textDelim':datastoreProperties.textDelim, 
          'decSep':datastoreProperties.decSep
        ]

  sql.eachRow(EXTRACT_DEF_STMT, map) { GroovyResultSet rss ->
    if (rss.ddl) {
      file.write rss.ddl?.asciiStream?.text
    }
  }  
}

def createFiles = {sql ->
  success = false
  properties = [:]

  for (srcstreamcode in STREAMS_LIST) {
    datastoreName = srcstreamcode
    modelCode = 'GDS_F_' + srcstreamcode[0..2]

    println "${modelCode}.${datastoreName}"

    results = getDatastoreProperties(modelCode, datastoreName)

    if (results.success) {
        createSrcstreamDefFile(sql, srcstreamcode, results.properties)
    } else {
      println "Cannot find file datastore for source stream ${srcstreamcode}!"
    }
  }
}

Sql.withInstance(dbConnParams) {
    Sql sql -> createFiles(sql)
}
