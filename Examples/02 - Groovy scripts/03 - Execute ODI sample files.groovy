/*
	Reads each .odi file from "04 - Demo scripts" folder and executes it
	using execAPI.executeODIScript method
*/

def folderName = "C:\\Data\\Projects\\DLAT project\\Examples\\01 - Demo scripts"
new File(folderName).eachFileMatch(~/.*.odi/) { file -> execodi.validate(file) }
new File(folderName).eachFileMatch(~/.*.odi/) { file -> execodi.execute(file) }
