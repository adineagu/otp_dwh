/*
 Execute an .odi file
*/

def folderName = "C:\\Data\\Projects\\DLAT project\\Examples\\01 - Demo scripts"
def scriptFile = "02 - Create datastores.odi"

def file = new File(folderName + "\\" + scriptFile)

execodi.validate(file)

execodi.execute(file)

