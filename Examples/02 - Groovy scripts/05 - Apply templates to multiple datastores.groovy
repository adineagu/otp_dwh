/*
    Apply template named "com.erstegroupit.dlat.oracle.odi.LoadCSVTable" to WPB datastores
*/

// retrieve all WPB datastores
wpbDatastores = metadata.getDatastores("com.erstegroupit.dlat.datastores.base.wpb.*")

wpbDatastores.each() {
    // apply template (i.e. resulting String contains ODI statements)
    odiStmt = codegen.apply(it, "com.erstegroupit.dlat.oracle.odi.LoadCSVTable")

    // validate statement
    execodi.validate(odiStmt)

    // execxute statement
    execodi.execute(odiStmt)
}
