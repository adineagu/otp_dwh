/*
    Apply template named "com.erstegroupit.dlat.oracle.odi.LoadCSVTable" to datastore
    "com.erstegroupit.dlat.oracle.odi.LoadCSVTable"
*/

// retrieve datastore metadata
amiDatastore = metadata.getDatastore("com.erstegroupit.dlat.datastores.base.fps.fpsfpsl")

// apply template (i.e. resulting String contains ODI statements)
odiStmt = codegen.apply(amiDatastore, "com.erstegroupit.dlat.oracle.odi.LoadCSVTable")

// validate statement
execodi.validate(odiStmt)

// execxute statement
execodi.execute(odiStmt)
