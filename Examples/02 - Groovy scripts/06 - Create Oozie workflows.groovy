/*
    This script can be used to create Oozie workflows for source stream definitions from an arbitray chosen source system
*/
def shell = new GroovyShell()

// get source stream definitions for a specific source system
metadata.getDocuments("dlat.datastore.base.crm").each { source ->

    // build IDatastore object from the source file
    def datastore = codegen.getASTObject(source)

    def jobName = "load_" + datastore.getAnnotation("TARGETTABLE").getParameters().get("TABLENAME").toLowerCase()
    def jobFolder = "C:\\Data\\Projects\\DLAT\\Hadoop\\Oozie_Jobs\\6.2.1\\" + jobName

    // apply a specific temmplate
    def stmt = codegen.apply(datastore, "dlat.datastore.etl.hadoop.CreateSrcstreamOozieJob_6_2_1")

    println "Create job " + jobName
    // execute the resulted Groovy code to create the workflow files
    shell.evaluate stmt 
    
    //def jobName = "load_lc_aut" + datastore.getBasename().toLowerCase()[3..6]

    buildCmd = jobFolder + "\\make.bat"
    // compile Scala code
    println buildCmd.execute().text

    archiveCmd = "C:\\PROGRA~1\\7-ZIP\\7Z a " + jobFolder + "\\..\\" + jobName + ".zip " + "C:\\Data\\Projects\\DLAT\\Hadoop\\Oozie_Jobs\\6.2.1\\" + jobName + "\\" + jobName
    // create an archive for deployment
    println archiveCmd.execute().text
}
