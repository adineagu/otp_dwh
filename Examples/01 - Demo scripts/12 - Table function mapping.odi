/*
Create an ODI mapping containing a table function component. In the example below the table function receives two
cursor parameters and a scalar parameter.

Limitation: the table functions should have the cursor parameters in the leading positions followed by scalar parameters.
            no interleaved parameter types are supported.

            Correct: table_function(
                       (select col1, col2 from tab1), 
                        123,
                        'abc'
                      )
            Wromg: table_function(
                       123,
                       (select col1, col2 from tab1),                        
                       'abc'
                      )

ODI global variable GLOBAL.BUSINESS_DATE, alphanumeric type, must be already defined
*/

DROP MAPPING IF EXISTS "HR_DEMO"."01_BASE_LOAD"."TABLE_FUNCTION"
;

CREATE MAPPING IF NOT EXISTS "HR_DEMO"."01_BASE_LOAD"."TABLE_FUNCTION"
PHYSICAL DESIGN "Default"
(
   OPTIMIZATION CONTEXT GLOBAL
   XKM "XKM SQL File Extract.GLOBAL" FOR TABLE CNT ,D ,S
   LKM "LKM SQL to Oracle (Built-In).GLOBAL" FOR TECHNOLOGY "File"
   (
      'DELETE_TEMPORARY_OBJECTS' = 'true'
   )
   IKM "IKM Oracle Insert.GLOBAL" FOR TABLE HR_BASE.COUNTRIES
   (
      'CREATE_TARGET_TABLE' = 'false',
      'INSERT_HINT' = 'APPEND PARALLEL',
      'SELECT_HINT' = ' ',
      'CONSTRAINTS' = 'None',
      'TRUNCATE_TARGET_TABLE' = 'false',
      'DELETE_ALL' = 'true',
      'INDEXES' = 'None'
   )
)
AS
INSERT INTO HR_BASE.COUNTRIES (
    COUNTRY_ID
   ,COUNTRY_NAME
   ,REGION_ID
   ,CURRENCY_CD
   ,BUSINESS_DATE
   ,LOAD_DATE
)
INTEGRATION TYPE 'Control Append'
UPDATE KEY COUNTRIES_PK
REJECT LIMIT 5 PERCENT
SELECT
    SRC.COUNTRY_ID as COUNTRY_ID
   ,COALESCE(SRC.COUNTRY_NAME,CNT.COUNTRY_NAME) as COUNTRY_NAME
   ,GREATEST(SRC.REGION_ID,(SELECT 23 FROM DUAL DUAL)) as REGION_ID
   ,SRC.CURRENCY_CD as CURRENCY_CD
   ,TO_DATE('#GLOBAL.BUSINESS_DATE','YYYY-MM-DD') as BUSINESS_DATE
   ,SYSDATE as LOAD_DATE
FROM
   TABLE(PKG.MYCOOLFUNCT(CURSOR
   (
   @properties(name="1st_cursor_param")
   SELECT
       S.COUNTRY_ID+20 as COUNTRY_ID
      ,S.COUNTRY_NAME || 'BBB' as COUNTRY_NAME
      ,S.REGION_ID
      ,R.REGION_NAME
      ,S.CURRENCY_CD
   FROM
      HR_SOURCE.COUNTRIES_FILE S
      JOIN HR_SOURCE.REGIONS_FILE R ON R.REGION_ID = S.REGION_ID
   WHERE
      R.REGION_NAME = 'Europe'
   ),CURSOR
   (
   @properties(name="2nd_cursor_param")
   SELECT
       D.DEPARTMENT_ID as DEPARTMENT_ID
      ,D.DEPARTMENT_NAME as DEPARTMENT_NAME
   FROM
      HR_SOURCE.DEPARTMENTS_FILE D
   WHERE
      D.DEPARTMENT_NAME = 'Accounting'
   ),123))  (COUNTRY_ID, COUNTRY_NAME, REGION_ID, CURRENCY_CD) AS SRC

   LEFT JOIN 
      HR_SOURCE.COUNTRIES_FILE CNT ON CNT.COUNTRY_ID = SRC.COUNTRY_ID
;
